<?php

if(!function_exists('cuda_setup')) {

	function cuda_setup() {

		add_theme_support('post-thumbnails');

		register_nav_menus(array(
			'main_menu' => __('Main Menu', 'cuda'),
		));
	}

}
add_action('after_setup_theme', 'cuda_setup');

function cuda_scripts() {

	wp_enqueue_style('cuda-style', get_stylesheet_uri());

	wp_enqueue_script('cuda-jquery', get_template_directory_uri().'/assets/js/jquery-3.2.1.min.js');
	wp_enqueue_script('cuda-criclebar', get_template_directory_uri().'/assets/js/circle-progress.min.js');
	wp_enqueue_script('cuda-jquery-ui', get_template_directory_uri().'/assets/js/jquery-ui.min.js');
	wp_enqueue_script('cuda-scripts', get_template_directory_uri().'/assets/js/scripts.js');
}
add_action('wp_enqueue_scripts', 'cuda_scripts');

function custom_post_types() {

	$portfolio_labels = array(
		'name' => 'Portfolio',
		'singular_name' => 'Portfolio',
		'add_new_item' => 'Add New Work',
		'edit_item' => 'Edit Work',
		'view_item' => 'View Work',

	);

	$portfolio_args = array(
		'labels' => $portfolio_labels,
		'public' => true,
		'publicly_queryable' => true,
		'query_var' => true,
		'menu_position' => 5,
		'menu_icon' => 'dashicons-images-alt2',
		'taxonomies' => array('portfolio_tags'),
		'capability_type' => 'post',
		'supports' => array('title', 'editor', 'thumbnail'),
		'rewrite' => true,
	);

	register_post_type('portfolio', $portfolio_args);

}
add_action('init', 'custom_post_types');

function custom_taxonomies() {

	$portfolio_labels = array(
		'name' => _x('Portfolio Tags', 'taxonomy general name', 'cuda'),
		'singular_name'     => _x( 'Portfolio Tag', 'taxonomy singular name', 'cuda' ),
	);

	$portfolio_args = array(
		'labels' => $portfolio_labels,
	);

	register_taxonomy('portfolio_tags', 'portfolio', $portfolio_args);

}
add_action('init', 'custom_taxonomies');

function cuda_widget_init() {
	
	register_sidebar(array(
		'name' => __( 'Main Sidebar', 'cuda' ),
		'id' => 'main-sidebar',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
	));
	
}
add_action('init', 'cuda_widget_init');

?>