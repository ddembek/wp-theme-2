<article class="post">
	
	<header class="post-header st-light">
		<a class="st-light" href="<?php the_permalink(); ?>"><?php the_title('<h1 class="post-title">','</h1>'); ?></a>
	</header>

	<div class="post-container row">
		<div class="post-left">
			<?php if(has_post_thumbnail()): ?>
				<div class="post-thumbnail">
					<?php the_post_thumbnail(); ?>
				</div>
			<?php endif; ?>
			<div class="post-info">
				<div class="post-author">Author: <?php the_author(); ?></div>
				<div class="post-date">Date: <?php the_date(); ?></div>
				<div class="post-tags"><?php the_tags('', ' '); ?></div>
			</div>
		</div>
		<div class="post-right">
			<?php the_content('Read More'); ?>
		</div>
	</div>

</article>