<form role="search" method="get" class="search-form" action="<?php echo esc_url( home_url( '/' ) ); ?>">
		<span class="section-title st-dark screen-reader-text"><?php echo _x( 'Search for', 'label', 'cuda' ); ?></span>
		<div class="line line-grey"></div>
		<input type="search" class="search-field" placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', 'cuda' ); ?>" value="<?php echo get_search_query(); ?>" name="s" />
	<button type="submit" class="btn-green st-light search-submit"><span class="screen-reader-text"><?php echo _x( 'Search', 'submit button', 'cuda' ); ?></span></button>
</form>