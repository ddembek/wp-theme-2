<?php
if(!is_active_sidebar('main-sidebar')): 
	return; 
endif;
?>

<aside class="sidebar st-dark">
	<?php dynamic_sidebar('main-sidebar'); ?>
</aside>
