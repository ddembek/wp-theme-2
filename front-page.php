<?php get_header(); ?>

<div class="welcome">
	<h1>Lorem ipsum dolor sit amet</h1>
	<h2 class="btn btn-orange">Work with us</h2>
</div>

<div class="services section-padding">
	<div class="container">
		<h1 class="section-title st-light">Services we provide</h1>
		<div class="line line-green"></div>
		<p class="section-p st-light">Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</p>
		<div class="row">
			<div class="service st-light">
				<img src="<?php echo get_template_directory_uri().'/assets/img/flag-icon.png'; ?>" alt="flag-icon">
				<h1>Branding</h1>
				<p>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</p>
			</div>
			<div class="service st-light">
				<img src="<?php echo get_template_directory_uri().'/assets/img/pencil-icon.png'; ?>" alt="flag-icon">
				<h1>Branding</h1>
				<p>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</p>
			</div>
			<div class="service st-light">
				<img src="<?php echo get_template_directory_uri().'/assets/img/tool-icon.png'; ?>" alt="flag-icon">
				<h1>Branding</h1>
				<p>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</p>
			</div>
			<div class="service st-light">
				<img src="<?php echo get_template_directory_uri().'/assets/img/rocket.png'; ?>" alt="flag-icon">
				<h1>Branding</h1>
				<p>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</p>
			</div>
		</div>
	</div>
</div>

<div class="team st-dark section-padding">
	<div class="container">
		<h1 class="section-title">Meet our team</h1>
		<div class="line line-grey"></div>
		<p class="section-p">Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</p>
		<div class="row">
			<div class="service">
				<img src="<?php echo get_template_directory_uri().'/assets/img/user.png'; ?>" alt="flag-icon">
				<h1>Lorem Ipsum</h1>
				<h2>CEO</h2>
				<p>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</p>
				<div class="row team-contact">
					<a href="#">
						<img src="<?php echo get_template_directory_uri().'/assets/img/fb.png'; ?>" alt="fb">
					</a>
					<a href="#">
						<img src="<?php echo get_template_directory_uri().'/assets/img/twitter.png'; ?>" alt="twitter">
					</a>
					<a href="#">
						<img src="<?php echo get_template_directory_uri().'/assets/img/linkedin.png'; ?>" alt="linkedin">
					</a>
					<a href="#">
						<img src="<?php echo get_template_directory_uri().'/assets/img/mail.png'; ?>" alt="mail">
					</a>
				</div>
			</div>
			<div class="service">
				<img src="<?php echo get_template_directory_uri().'/assets/img/user.png'; ?>" alt="flag-icon">
				<h1>Lorem Ipsum</h1>
				<h2>Creative Director</h2>
				<p>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</p>
				<div class="row team-contact">
					<a href="#">
						<img src="<?php echo get_template_directory_uri().'/assets/img/fb.png'; ?>" alt="fb">
					</a>
					<a href="#">
						<img src="<?php echo get_template_directory_uri().'/assets/img/twitter.png'; ?>" alt="twitter">
					</a>
					<a href="#">
						<img src="<?php echo get_template_directory_uri().'/assets/img/linkedin.png'; ?>" alt="linkedin">
					</a>
					<a href="#">
						<img src="<?php echo get_template_directory_uri().'/assets/img/mail.png'; ?>" alt="mail">
					</a>
				</div>
			</div>
			<div class="service">
				<img src="<?php echo get_template_directory_uri().'/assets/img/user.png'; ?>" alt="flag-icon">
				<h1>Lorem Ipsum</h1>
				<h2>Lead Designer</h2>
				<p>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</p>
				<div class="row team-contact">
					<a href="#">
						<img src="<?php echo get_template_directory_uri().'/assets/img/fb.png'; ?>" alt="fb">
					</a>
					<a href="#">
						<img src="<?php echo get_template_directory_uri().'/assets/img/twitter.png'; ?>" alt="twitter">
					</a>
					<a href="#">
						<img src="<?php echo get_template_directory_uri().'/assets/img/linkedin.png'; ?>" alt="linkedin">
					</a>
					<a href="#">
						<img src="<?php echo get_template_directory_uri().'/assets/img/mail.png'; ?>" alt="mail">
					</a>
				</div>
			</div>
			<div class="service">
				<img src="<?php echo get_template_directory_uri().'/assets/img/user.png'; ?>" alt="flag-icon">
				<h1>Lorem Ipsum</h1>
				<h2>Developer</h2>
				<p>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</p>
				<div class="row team-contact">
					<a href="#">
						<img src="<?php echo get_template_directory_uri().'/assets/img/fb.png'; ?>" alt="fb">
					</a>
					<a href="#">
						<img src="<?php echo get_template_directory_uri().'/assets/img/twitter.png'; ?>" alt="twitter">
					</a>
					<a href="#">
						<img src="<?php echo get_template_directory_uri().'/assets/img/linkedin.png'; ?>" alt="linkedin">
					</a>
					<a href="#">
						<img src="<?php echo get_template_directory_uri().'/assets/img/mail.png'; ?>" alt="mail">
					</a>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="skills st-dark section-padding">
	<div class="container">
		<h1 class="section-title">We got skills</h1>
		<div class="line line-lightgrey"></div>
		<p class="section-p">Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</p>
		<div class="row">
			<div class="circle">
				<div id="circle-1">
					<span class="circle-value">90%</span>
				</div>
				<span class="st-dark">Web Design</span>
			</div>
			<div class="circle">
				<div id="circle-2">
					<span class="circle-value">75%</span>
				</div>
				<span class="st-dark">Html/Css</span>
			</div>
			<div class="circle">
				<div id="circle-3">
					<span class="circle-value">70%</span>
				</div>
				<span class="st-dark">Graphic Design</span>
			</div>
			<div class="circle">
				<div id="circle-4">
					<span class="circle-value">85%</span>
				</div>
				<span class="st-dark">UI/UX</span>
			</div>
		</div>
	</div>
</div>

<div class="portfolio st-dark section-padding">
	<div class="container">
		<h1 class="section-title">We got skills</h1>
		<div class="line line-yellow"></div>
		<p class="section-p">Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</p>
		<div id="tabs">
			<?php

			$portfolio_tags = get_terms(array('taxonomy' => 'portfolio_tags'));
			$i = 2;
			$j = 2;

			?>
			<ul class="portfolio-tags row">
				<li><a class="portfolio-tag active-tab" href="#work-1">All Works</a>
				<?php foreach($portfolio_tags as $tag): ?>
					<li><a class="portfolio-tag" href="#work-<?php echo $i; ?>"><?php echo $tag->name; ?></a></li>
				<?php $i++; endforeach; ?>
			</ul>
			<div class="row works">
				<?php 

				$portfolio_all_loop = new WP_Query(array('post_type' => 'portfolio', 'posts_per_page' => 4));

				if($portfolio_all_loop->have_posts()):

				?>
				<div class="work" id="work-1">

				<?php 
					while($portfolio_all_loop->have_posts()): $portfolio_all_loop->the_post();
				?>
					<div class="work-content">
						<?php if(has_post_thumbnail()): ?>
							<div class="work-thumbnail">
								<?php the_post_thumbnail(); ?>
							</div>
						<?php endif; ?>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</div>
					
				<?php
					endwhile; wp_reset_postdata(); ?>
				</div>
				
				<?php

				foreach($portfolio_tags as $tag):

					$portfolio_loop = new WP_Query(array(
						'post_type' => 'portfolio',
						'posts_per_page' => 4,
						'tax_query' => array(
							array(
								'taxonomy' => 'portfolio_tags',
								'field' => 'name',
								'terms' => $tag,
							),
						),
					)); ?>
					<div class="work" id="work-<?php echo $j; ?>">
				<?php 
					while($portfolio_loop->have_posts()): $portfolio_loop->the_post();
				?>
					<div class="work-content">
						<?php if(has_post_thumbnail()): ?>
							<div class="work-thumbnail">
								<?php the_post_thumbnail(); ?>
							</div>
						<?php endif; ?>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</div>
					
				<?php
					endwhile; wp_reset_postdata(); $j++; ?>
				</div>
				<?php endforeach; ?>
			<?php else: ?>
				<p>Juz wkrotce sie cos tu pojawi</p>
			<?php endif; ?>
			</div>
		</div>
		<a class="btn btn-green" href="<?php echo esc_url( home_url( '/portfolio' ) ); ?>">View More Projects</a>
	</div>
</div>

<div class="testimonials st-light section-padding">
	<div class="container">
		<h1 class="section-title">What people say about us</h1>
		<div class="line line-pink"></div>
		<p class="section-p">Our Client love us</p>
		<?php echo do_shortcode('[testimonial_view id=2]'); ?>
	</div>
</div>

<div id="test"></div>



<?php get_footer(); ?>