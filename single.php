<?php get_header(); ?>

<main class="blog-posts section-padding">
	<div class="container">
		<?php

		while(have_posts()): the_post();

			get_template_part('template-parts/content');

		endwhile;

		if ( comments_open() || get_comments_number() ) {
			echo '<div class="comments-area">';
			comments_template();
			echo '</div>';
		}

		?>
	</div>
</main>

<?php get_footer(); ?>