	<footer class="contact st-light section-padding">
		<div class="container">
			<h1 class="section-title">Get in Touch</h1>
			<div class="line line-blue"></div>
			<p class="section-p">Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</p>
			<?php echo do_shortcode('[contact-form-7 id="261" title="Cuda Contact Form"]'); ?>
		</div>
		<div class="social-media">
			<div class="container">
				<ul class="row">
					<li><a href="#">Facebook</a>
					<li><a href="#">Twitter</a>
					<li><a href="#">Google+</a>
					<li><a href="#">LinkedIn</a>
					<li><a href="#">Behance</a>
					<li><a href="#">Dribbble</a>
					<li><a href="#">GitHub</a>
				</ul>
			</div>
		</div>
	</footer>

	<?php wp_footer(); ?>
	</body>
</html>