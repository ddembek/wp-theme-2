<?php get_header(); ?>

<main class="blog-posts section-padding">
	<div class="container">
		<?php

		while(have_posts()): the_post();

			get_template_part('template-parts/content');

		endwhile;

		?>
	</div>
</main>


<?php get_footer(); ?>