<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1" />
	<meta name="viewport" content="width=device-width">
	<meta name="description" content="<?php bloginfo('descriptio'); ?>">
	<title><?php bloginfo('title'); ?></title>
	<link href="https://fonts.googleapis.com/css?family=Titillium+Web:200,200i,300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">
	<script src="https://cdn.jsdelivr.net/combine/npm/html5shiv@3.7.3/dist/html5shiv-printshiv.min.js,npm/html5shiv@3.7.3,npm/css3-mediaqueries-js@1.0.0/css3-mediaqueries.min.js"></script>
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div class="top-scroll">
	<img src="<?php echo get_template_directory_uri().'/assets/img/arrow.png'; ?>" alt="arrow">
</div>

<div class="main-header">
	<div class="container">
		<nav class="navbar row">
			<h1 class="site-title"><?php bloginfo('title'); ?></h1>
			<div class="mobile-menu">
				<button class="mobile-menu-button st-light">Menu</button>
				<?php
				wp_nav_menu(array(
					'container' => ' ',
					'menu_class' => 'main-menu row',
					'depth' => 1,
				));
				?>
			</div>
		</nav>
	</div>
</div>
