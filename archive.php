<?php get_header(); ?>

<main class="blog-posts section-padding">

	<div class="container">
		<header class="archive-header">
			<h1 class="st-dark section-title"><?php the_archive_title(); ?></h1>
			<div class="line line-grey"></div>
		</header>

		<?php

		if(have_posts()):

			while(have_posts()): the_post();

				get_template_part('template-parts/content');

			endwhile;

			the_posts_pagination( array(
				'prev_text' => '&laquo',
				'next_text' => '&raquo',
			) );

		else: 

			get_template_part('template-parts/content', 'none');

		endif;

		?>

	</div>

</main>

<?php get_footer(); ?>