$(function() {
	$('#tabs').tabs({show: 400, hide: 400});

	$(".top-scroll").click(function () {
	    $("body,html").animate({scrollTop: 0});
	});

	$(window).scroll(function () {

	    if ($(window).scrollTop() > 200) {

	        $(".top-scroll").show(400);

	    } else {

	        $(".top-scroll").hide(400);

	    }

	});

	$('.portfolio-tags li').click(function() {
		$(this).find('a').addClass('active-tab');
		$(this).siblings().find('a').removeClass('active-tab');
	});

	$('.mobile-menu-button').click(function() {

		var menu_display = $('.main-menu').css('display');

		if(menu_display == 'none') {
			$('.main-menu').show('slow');
			$('.mobile-menu-button').css({'border-bottom-left-radius':0, 'border-bottom-right-radius':0});
		} else {
			$('.main-menu').hide('slow');
			$('.mobile-menu-button').css({'border-bottom-left-radius':'5px', 'border-bottom-right-radius':'5px'});
		}

		$(window).resize(function() {
			var window_width = $(window).width();

			if(window_width >= 768) {
				$('.main-menu').css('display', 'block');
			}
		});
			
	});


	 $('#circle-1').circleProgress({
	    value: 0.9,
	    size: 150,
	    thickness: 15,
	    fill: {
	      color: "#30bae8"
	    },
	    emptyFill: "#dceaed",
	    animation: false
	  });

	 $('#circle-2').circleProgress({
	    value: 0.75,
	    size: 150,
	    thickness: 15,
	    fill: {
	      color: "#d63c7a"
	    },
	    emptyFill: "#dceaed",
	    animation: false
	  });

	 $('#circle-3').circleProgress({
	    value: 0.7,
	    size: 150,
	    thickness: 15,
	    fill: {
	      color: "#15c6a9"
	    },
	    emptyFill: "#dceaed",
	    animation: false
	  });

	 $('#circle-4').circleProgress({
	    value: 0.85,
	    size: 150,
	    thickness: 15,
	    fill: {
	      color: "#eb7d4a"
	    },
	    emptyFill: "#dceaed",
	    animation: false
	  });

});
