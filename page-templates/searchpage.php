<?php

/*
	Template Name: Search Page

*/

?>
<?php get_header(); ?>

<main class="blog-posts section-padding">
	<div class="container">
		<?php get_search_form(); ?>
	</div>
</main>

<?php get_footer(); ?>
