<?php
/* Template Name: Portfolio */
?>

<?php get_header(); ?>

<main class="blog-posts section-padding">
	<div class="container">
		<header class="portfolio-page">
			<h1 class="section-title st-dark">Portfolio</h1>
		</header>
		<div class="line line-grey"></div>
		<div class="portfolio-page-works row">
			<?php

			$portfolio_loop = new WP_Query(array('post_type' => 'portfolio', 'posts_per_page' => -1));

			if($portfolio_loop->have_posts()): 

				while($portfolio_loop->have_posts()): $portfolio_loop->the_post();
			?>
				<article class="portfolio-page-work">
					<header>
						<?php the_title('<h1 class="st-light section-title">', '</h1>'); ?>
					</header>
					<div class="portfolio-page-thumbnail">
						<?php if(has_post_thumbnail()): ?>
							<?php the_post_thumbnail(); ?>
						<?php else: ?>
							<img src="<?php echo get_template_part().'/assets/img/'; ?>">
						<?php endif; ?>
					</div>
					<div class="portfolio-page-content">
						<p class="st-dark"><?php echo wp_trim_words(get_the_content(), '25', '...'); ?></p>
						<a class="st-light btn-orange section-title" href="<?php the_permalink(); ?>">Read More</a>
					</div>
				</article>
			<?php

				endwhile; wp_reset_postdata();

			else: 

				get_template_part('template-parts/content', 'none');

			endif;

			?>
		</div>
	</div>
</main>

<?php get_footer(); ?>