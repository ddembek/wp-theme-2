<?php get_header(); ?>

<main class="blog-posts section-padding">
	<div class="search-content container">
		<header class="search-header st-dark">
			<h1><?php printf( __( 'Search Results for: %s', 'twentysixteen' ), '<span>' . esc_html( get_search_query() ) . '</span>' ); ?></h1>
		</header>
		<div class="row">
			<div class="posts">
			<?php

			if(have_posts()):

				while(have_posts()): the_post();

					get_template_part('template-parts/content');

				endwhile;

				the_posts_pagination( array(
					'prev_text' => '&laquo',
					'next_text' => '&raquo',
				) );

			else:

				get_template_part('template-parts/content', 'none');

			endif;

			?>
			</div>

			<?php get_sidebar(); ?>

		</div>
	</div>
</main>


<?php get_footer(); ?>